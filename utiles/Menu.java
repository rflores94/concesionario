package utiles;

/**
 * Implementa la clase Menu que sirva para gestionar un menú.
 * 
 * Un ejemplo de menú es:
 ** 
 * Plantilla de la empresa (1) Añadir Becario (2) Añadir Empleado (3) Listar
 * plantilla (4) Listar becarios (5) Listar empleados (6) Contar becarios (7)
 * Contar empleados (8) Salir
 * 
 * Básicamente, un menú ha de:
 * 
 * Mostrar unas opciones
 * Recoger y devolver las opciones indicadas
 * 
 * A continuación se indican los miembros de la clase Menu:
 * 
 * Título del menú. Se mostrará al principio del menú.Opciones del menú. Cada
 * una de estas opciones es una cadena que se mostrará en una línea aparte.
 * Número de opciones del menú. Un método mostrar() que muestre el menú,
 * añadiéndole a cada elemento del menú un número, comenzando por 1. Un método
 * recogeOpcionValida() que recoja una de las opciones válidas (entre 1 y número
 * de opciones del menú) Un método gestionar() que gestione el menú. Gestionar
 * un menú consiste en mostrarlo y recoger la opción válida del menú. Recuerda
 * el constructor, al que se le pasarán como argumento tanto el título como las
 * opciones del menú.
 * 
 * Una vez implementada la clase menú, utilízala para gestionar la clase Matriz.
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 */

public class Menu {
	private String titulo;
	private String[] opciones;
	
	public Menu(String titulo, String[] opciones) {
		this.titulo=titulo;
		this.opciones=opciones;
	}
	
	private void mostrar(){
		int indice=1;
		System.out.println(titulo);
		for (String opcion : opciones) {
			System.out.println("("+indice++ +") "+opcion);
		}
	}
	
	public int gestionar(){
		int numOpcion;
		do{
		mostrar();
		numOpcion=Teclado.leerEntero("Dime la opcion que quieres realizar. Debe ser mayor que 0 y menor que "+(opciones.length+1)+".");
		}while(recogeOpcionValida(numOpcion));
		return numOpcion;
	}

	private boolean recogeOpcionValida(int numOpcion) {
		if(numOpcion<1 || numOpcion>opciones.length)
			return true;
		return false;
	}
}
