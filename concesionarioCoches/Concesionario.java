package concesionarioCoches;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Clase Concesionario, envoltorio de nuestro ArrayList
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 */

public class Concesionario {
	/**
	 * ArrayList donde guardaremos los Coches
	 */
	static ArrayList<Coche> concesionario = new ArrayList<Coche>();
	
	/**
	 * A�ade un Coche a nuestro concesionario.
	 * @param coche
	 * @throws CocheYaExisteException
	 * 								Si el coche que le pasamos ya existe.
	 */
	void annadir(Coche coche) throws CocheYaExisteException{
		if(concesionario.contains(coche))
			throw new CocheYaExisteException("Ya existe un coche con esa matricula.");
		concesionario.add(coche);
	}
	
	/**
	 * Elimina un Coche de nuestro concesionario.
	 * @param coche
	 * @throws CocheNoExisteException
	 * 								Si el Coche que le pasamos no existe.
	 * @throws ConcesionarioVacioException 
	 * 										Si el Concesionario esta vacio.
	 */
	void eliminar(Coche coche) throws CocheNoExisteException, ConcesionarioVacioException{
		if(concesionario.isEmpty())
			throw new ConcesionarioVacioException("El concesionario esta vacio.");
		if(!concesionario.contains(coche))
			throw new CocheNoExisteException("No existe un coche con esa matricula.");
		concesionario.remove(concesionario.get(concesionario.indexOf(coche)));
	}
	
	/**
	 * Muestra el numero de coches en nuestro concesionario.
	 * @return
	 * 			Cadena con el numero de coches.
	 * @throws ConcesionarioVacioException 
	 * 										Si el concesionario esta vacio.
	 */
	String contarCoches() throws ConcesionarioVacioException{
		if(concesionario.isEmpty())
			throw new ConcesionarioVacioException("El concesionario esta vacio."); 
		return "Hay " + concesionario.size() + " coches en nuestro concesionario.";
	}
	
	/**
	 * Muestra un coche que le pasamos.
	 * @param coche
	 * @return
	 * 			Cadena con el Coche.
	 * @throws CocheNoExisteException
	 * 								Si el Coche que le pasamod no existe.
	 * @throws ConcesionarioVacioException 
	 * 										Si el concesionario esta vacio.
	 */
	String mostrarCoche(Coche coche) throws CocheNoExisteException, ConcesionarioVacioException{
		if(concesionario.isEmpty())
			throw new ConcesionarioVacioException("El concesionario esta vacio."); 
		if(!concesionario.contains(coche))
			throw new CocheNoExisteException("No existe un coche con esa matricula.");
		return concesionario.get(concesionario.indexOf(coche)).toString();
	}
	
	/**
	 * Muestra todos los coches del color que le pasamos.
	 * @param color
	 * 				Color del que mostraremos los Coches.
	 * @return
	 * 			ArrayList con los coches del color introducido.
	 * @throws ConcesionarioVacioException 
	 * 										Si el concesionario esta vacio.
	 */
	String  mostrarCochesPorColor(Colores color) throws ConcesionarioVacioException{
		if(concesionario.isEmpty())
			throw new ConcesionarioVacioException("El concesionario esta vacio."); 
		String mensaje = "";
		Iterator<Coche> it = concesionario.listIterator();
		while(it.hasNext()){
			Coche coche = (Coche) it.next();
			if(coche.getColor() == color)
				mensaje += coche.toString() + "\n";
		}
		return mensaje;
	}

	/**
	 * Muestra todos los coches del color que le pasamos.
	 * @param color
	 * 				Color del que mostraremos los Coches.
	 * @return
	 * 			ArrayList con los coches del color introducido.
	 * @throws ConcesionarioVacioException 
	 * 										Si el concesionario esta vacio.
	 */
	String  mostrarCoches() throws ConcesionarioVacioException{
		if(concesionario.isEmpty())
			throw new ConcesionarioVacioException("El concesionario esta vacio."); 
		String mensaje = "";
		Iterator<Coche> it = concesionario.listIterator();
		while(it.hasNext()){
			mensaje += it.next().toString() + "\n";
		}
		return mensaje;
	}
	
}
