package concesionarioCoches;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Clase Coche
 * 
 * @author Roberto Carlos Flores Gomez
 * @verion 1.0
 */

public class Coche {
	private String matricula;
	private Modelo modelo;
	private Colores color;
	private static final String MATRICULA_VALIDA = "^\\d{4}[ -]?[B-Z&&[^EIOUQ]]{3}$";
	
	Coche(String matricula, Modelo modelo,  Colores colores) throws CocheInvalidoException {
		setMatricula(matricula);
		setModelo(modelo);
		setColor(colores);
	}
	
	Coche(String matricula) throws CocheInvalidoException{
		setMatricula(matricula);
	}
	
	private boolean comprobarMatricula(String matricula){
		Pattern p = Pattern.compile(MATRICULA_VALIDA);
		Matcher m = p.matcher(matricula);
		if(m.find())
			return true;
		return false;
	}

	private String getMatricula() {
		return matricula;
	}

	private void setMatricula(String matricula) throws CocheInvalidoException {
//		if(!comprobarMatricula(matricula))
		if(!Pattern.matches(MATRICULA_VALIDA, matricula))
			throw new CocheInvalidoException("La matricula introducida no es valida.");
		this.matricula = matricula;
	}

	private Modelo getModelo() {
		return modelo;
	}

	private void setModelo(Modelo modelo) throws CocheInvalidoException {
		if(modelo == null)
			throw new CocheInvalidoException("El modelo no puede ser nulo.");
		this.modelo = modelo;
	}

	Colores getColor() {
		return color;
	}

	private void setColor(Colores color) throws CocheInvalidoException {
		if(color == null)
			throw new CocheInvalidoException("El color no puede ser nulo.");
		this.color = color;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coche other = (Coche) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Coche: Matricula: " + matricula + "\nModelo: " + modelo.getMarca() + " " + modelo + "\nColor: " + color + ".\n";
	}
	
	
}
