package concesionarioCoches;

/**
 * Enumeracion de los modelos de los coches
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 */
public enum Modelo {
	SERIE1 (Marca.BMW),
	SERIE3 (Marca.BMW),
	SERIE5 (Marca.BMW),
	IBIZA (Marca.SEAT),
	TOLEDO (Marca.SEAT),
	CORDOBA (Marca.SEAT);
	
	private Marca marca;
	private static final Modelo[] VALUES = values();
	
	Modelo(Marca marca){
		setMarca(marca);
	}

	Marca getMarca() {
		return marca;
	}

	private void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	static String[] opcionesModelo(){
		String[] modelos = new String[VALUES.length];
		for (int i = 0; i < modelos.length; i++)
			modelos[i] = VALUES[i].name();
		return modelos;
	}
}
