package concesionarioCoches;
/**
 * Enumeracion de los colores de los coches.
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 */

public enum Colores {
	PLATA,
	ROJO,
	AZUL;
}
