package concesionarioCoches;

public class ConcesionarioVacioException extends Exception {

	public ConcesionarioVacioException(String string) {
		super(string);
	}

}
