package concesionarioCoches;
/**
 * Enumeracion de las marcas de los coches.
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 */
public enum Marca {
	BMW, SEAT;
}
