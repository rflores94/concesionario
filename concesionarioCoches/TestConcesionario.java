package concesionarioCoches;

import utiles.*;

/**
 * TestConcesionario
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 */
public class TestConcesionario {
	private static final Menu menuPrincipal = new Menu("Concesionario de coches", new String[] { "Annadir coche", "Borrar coche",
			"Buscar coche", "Mostrar coches", "Mostrar coches por color", "Contar coches", "Salir" });
	private static Concesionario concesionario = new Concesionario();
	public static void main(String[] args) {
		while(true)
			mostrarMenu();
	}

	private static void mostrarMenu(){
		switch (menuPrincipal.gestionar()) {
		case 1:
			annadirCoche();
			break;
		case 2:
			eliminarCoche();
			break;
		case 3:
			buscarCoche();
			break;
		case 4:
			mostrarCoches();
			break;
		case 5:
			mostrarPorColor();
			break;
		case 6:
			contarCoches();
			break;
		case 7:
			System.exit(0);

		}
	}

	/**
	 * Muestra cuantos coches hay en el Concesionario
	 */
	private static void contarCoches() {
		try {
			System.out.println(concesionario.contarCoches());
		} catch (ConcesionarioVacioException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Muestra todos los coches del color que le pasemos
	 */
	private static void mostrarPorColor() {
		try {
			System.out.println(concesionario.mostrarCochesPorColor(pedirColor()));
		} catch (ConcesionarioVacioException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Muestra todos los coches
	 */
	private static void mostrarCoches() {
		try {
			System.out.println(concesionario.mostrarCoches());
		} catch (ConcesionarioVacioException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Busca un coche por la matricula
	 */
	private static void buscarCoche() {
		try {
			System.out.println(concesionario.mostrarCoche(new Coche(Teclado.leerCadena("Dime la matricula del coche: "))));
		} catch (CocheNoExisteException | CocheInvalidoException | ConcesionarioVacioException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Elimina un coche por la matricula
	 */
	private static void eliminarCoche() {
		try {
			concesionario.eliminar(new Coche(Teclado.leerCadena("Dime la matricula del coche: ")));
		} catch (CocheNoExisteException | CocheInvalidoException | ConcesionarioVacioException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * A�ade un coche
	 */
	private static void annadirCoche() {
		try {
			concesionario.annadir(new Coche(Teclado.leerCadena("Dime la matricula del coche: "), pedirModelo(), pedirColor()));
		} catch (CocheYaExisteException | CocheInvalidoException  e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Pide el color del coche
	 * @return
	 * 			Color
	 */
	private static Colores pedirColor() {
		Colores[] colores = Colores.values();
		int opcion=0;
		do{
			for (int i = 0; i < colores.length; i++) {
				System.out.println(i+1 +". " + colores[i]);
			}
			opcion = Teclado.leerEntero("Dime el color del coche: ");
		}while(opcion<1 || opcion>colores.length);
		return (Colores)colores[opcion-1];
	}

	/**
	 * Pide el modelo del coche
	 * @return
	 * 			Modelo
	 */
	private static Modelo pedirModelo() {
		Modelo[] modelos = Modelo.values();
		int opcion=0;
		do{
			for (int i = 0; i < modelos.length; i++) {
				System.out.println(i+1 +". " + modelos[i]);
			}
			opcion = Teclado.leerEntero("Dime el modelo del coche: ");
		}while(opcion<1 || opcion>modelos.length);
		return (Modelo)modelos[opcion-1];
	}
}
