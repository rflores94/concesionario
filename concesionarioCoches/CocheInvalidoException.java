package concesionarioCoches;

public class CocheInvalidoException extends Exception {

	public CocheInvalidoException(String string) {
		super(string);
	}
	
}
